#ifndef IS_INCLUDEGL_H
#define IS_INCLUDEGL_H

#ifndef HAVE_SDL
#include <GL/glut.h>
#else
#include <SDL.h>
#include <SDL_thread.h>
#ifdef HAVE_SDL_MIXER
#include <SDL_mixer.h>
#endif
#ifdef HAVE_SDL_NET
#include <SDL_net.h>
#endif
#endif

#include <GL/gl.h>
#include <GL/glu.h>

#ifdef _MSC_VER
  #pragma warning(disable:4786) // workaround for MSVC6 bug, needs service pack
#endif

#endif
